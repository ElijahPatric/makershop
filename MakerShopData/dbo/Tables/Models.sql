﻿CREATE TABLE [dbo].[Models](

[ModelID]			INT			IDENTITY(1,1) NOT NULL,
[ModelName]			NVARCHAR (50)			NULL,
[Size]				INT						NULL,
[UserID]			INT						NOT NULL,
[Download]			BIT DEFAULT 0			NOT NULL,
[ImagePath] NVARCHAR(50) NULL, 
   
    CONSTRAINT ModelPK		PRIMARY KEY (ModelID),


CONSTRAINT			UserFK			FOREIGN KEY(UserID)
							REFERENCES Users(UserID)
							ON UPDATE NO ACTION
							ON DELETE NO ACTION
);