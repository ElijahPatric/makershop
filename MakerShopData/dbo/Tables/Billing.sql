﻿CREATE TABLE [dbo].[Billing](

[BillingID]			INT			IDENTITY(1,1) NOT NULL,
[AddressLine1]			NVARCHAR (50)			NULL,
[AddressLine2]			NVARCHAR (50)			NULL,
[ZipCode]				NVARCHAR (15)			NULL,
[UserID]			INT						NOT NULL,

CONSTRAINT BillingPK		PRIMARY KEY (BillingID),

CONSTRAINT			UserFK5			FOREIGN KEY(UserID)
							REFERENCES Users(UserID)
							ON UPDATE NO ACTION
							ON DELETE NO ACTION

);