﻿CREATE TABLE [dbo].[Users](

[UserID]			INT			IDENTITY(1,1) NOT NULL,
[UserName]			NVARCHAR (50)			NULL,
[LastName]			NVARCHAR (50)			NULL,
[FirstName]			NVARCHAR (50)			NULL,
[Role]				NVARCHAR (12)			NULL,
[Email]				NVARCHAR (50)			NULL,
[Phone]				NVARCHAR (20)			NULL,
[Password]			NVARCHAR (30)			NULL,

CONSTRAINT UserPK		PRIMARY KEY (UserID)

);