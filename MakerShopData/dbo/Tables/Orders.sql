﻿CREATE TABLE [dbo].[Orders](

[OrderID]			INT			IDENTITY(1,1) NOT NULL,
[TotalCost]			MONEY		NULL,
[ShippingCost]		MONEY		NULL,
[Tax]				MONEY		NULL,
[DateOrdered]		NVARCHAR (12)	NULL,
[DateShipped]		NVARCHAR (12)	NULL,
[UserID]			INT						NOT NULL,

[ModelID] INT NULL, 
    [isDownload] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT OrderPK		PRIMARY KEY (OrderID),



CONSTRAINT			UserFK3			FOREIGN KEY(UserID)
							REFERENCES Users(UserID)
							ON UPDATE NO ACTION
							ON DELETE NO ACTION,

CONSTRAINT			ModelFK6			FOREIGN KEY(ModelID)
							REFERENCES Models(ModelID)
							ON UPDATE NO ACTION
							ON DELETE NO ACTION

);