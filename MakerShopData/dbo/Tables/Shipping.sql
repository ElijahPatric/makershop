﻿CREATE TABLE [dbo].[Shipping](

[ShippingID]			INT			IDENTITY(1,1) NOT NULL,
[AddressLine1]			NVARCHAR (50)			NULL,
[AddressLine2]			NVARCHAR (50)			NULL,
[ZipCode]				NVARCHAR (15)			NULL,
[UserID]			INT						NOT NULL,

CONSTRAINT ShippingPK		PRIMARY KEY (ShippingID),

CONSTRAINT			UserFK4			FOREIGN KEY(UserID)
							REFERENCES Users(UserID)
							ON UPDATE NO ACTION
							ON DELETE NO ACTION

);